


### Python packages to install

```
python -m pip install --upgrade aiohttp aiofile click jinja2 watchdog
```

### Run server

Specify the port to listen on and the Kubernetes namespace to monitor.

```bash
cd this_repo
python -m demo_server server --port 8000
```

The uploaded images will be stored as `jobs/{time_ns}/input`.