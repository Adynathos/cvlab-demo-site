
import asyncio
import click
import logging
# import cv2 as cv
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler, LoggingEventHandler

log = logging.getLogger(__name__)

class LoggingEventHandler(FileSystemEventHandler):
	"""Logs all the events captured."""

	def on_moved(self, event):
		super(LoggingEventHandler, self).on_moved(event)

		what = 'directory' if event.is_directory else 'file'
		logging.info("Moved %s: from %s to %s", what, event.src_path,
					 event.dest_path)

	def on_created(self, event):
		super(LoggingEventHandler, self).on_created(event)

		what = 'directory' if event.is_directory else 'file'
		logging.info("Created %s: %s", what, event.src_path)

	def on_deleted(self, event):
		super(LoggingEventHandler, self).on_deleted(event)

		what = 'directory' if event.is_directory else 'file'
		logging.info("Deleted %s: %s", what, event.src_path)

	def on_modified(self, event):
		super(LoggingEventHandler, self).on_modified(event)

		what = 'directory' if event.is_directory else 'file'
		logging.info("Modified %s: %s", what, event.src_path)

@click.command('compute')
# @click.option('--namespace', type=str, help="Kubernetes namespace to monitor")
def main():
	"""
	Display the queue in console.
	"""
	logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
	event_handler = LoggingEventHandler()
	observer = Observer()
	observer.schedule(event_handler, './jobs', recursive=True)
	observer.start()

	asyncio.get_event_loop().run_until_complete(asyncio.sleep(180))
	
