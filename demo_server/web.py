
import asyncio
import json, yaml
import logging
import click
from datetime import datetime, date
from pathlib import Path
from aiohttp import web
import aiofile
import time
from .save_upload_file import save_upload_file

log = logging.getLogger(__name__)

MAX_UPLOAD_SIZE = 1*1024*1024 # 1 MB

def json_serialize_unknown(obj):
	if isinstance(obj, (datetime, date)):
		return obj.isoformat()
	
	raise TypeError(f'Object not serializable class={type(obj)} obj={obj}')


class WatchdogWebServer:

	WEB_STATIC_DIR = Path(__file__).parent / 'web_assets'
	WEB_STATIC_INDEX = WEB_STATIC_DIR / 'index.html'

	def __init__(self, port=8000):
		self.port = port
		self.pod_hierarchy_json = '[]'

		# setup webserver
		self.application = web.Application()

		self.application.add_routes([
			web.get('/', self.web_index),
			web.post('/jobs/submit', self.web_jobs_submit),
			web.static('/static', self.WEB_STATIC_DIR / 'static', follow_symlinks=True),
		])

	async def web_index(self, request):
		return web.FileResponse(self.WEB_STATIC_INDEX)

	
	async def web_jobs_submit(self, request):
		"""
		Upload file to aiohttp:
		https://docs.aiohttp.org/en/stable/web_quickstart.html#file-uploads

		Write file async in python:
		https://github.com/mosquito/aiofile#write-and-read-with-helpers
		"""	


		request_reader = await request.multipart()
		field = await request_reader.next()
		assert field.name == 'input_image'

		log.debug(f'submission filename: {field.filename}')
		
		
		JOB_DIR = Path('./jobs')
		dest_dir = JOB_DIR / f'{time.time_ns()}'
		dest_dir.mkdir(parents=True)
		dest_path = dest_dir / 'input'

		await save_upload_file(field, dest_path, MAX_UPLOAD_SIZE)
		
		return web.json_response(dict(
			result = 'ok'
		))


		# async for field in await request.multipart():
		# 	log.debug(f'field {field.name}')

    # # reader.next() will `yield` the fields of your form

    # field = await reader.next()
    # assert field.name == 'name'
    # name = await field.read(decode=True)

    # field = await reader.next()
    # assert field.name == 'mp3'
    # filename = field.filename
    # # You cannot rely on Content-Length if transfer is chunked.
    # size = 0
    # with open(os.path.join('/spool/yarrr-media/mp3/', filename), 'wb') as f:
    #     while True:
    #         chunk = await field.read_chunk()  # 8192 bytes by default.
    #         if not chunk:
    #             break
    #         size += len(chunk)
    #         f.write(chunk)

    # return web.Response(text='{} sized of {} successfully stored'
    #                          ''.format(filename, size))

	def run_blocking(self):
		log.info('Server being constructed')

		# blocking run
		web.run_app(self.application)

		# runner = web.AppRunner(self.application)
		# await runner.setup()
		# site = web.TCPSite(runner, '0.0.0.0', self.port)
		
		# log.info('Server starting')

		# # wait for both
		# await asyncio.gather(
		# 	self.monitor.run(),
		# 	site.start(),
		# )

@click.command('server')
@click.option('--port', type=int, default=8000)
@click.option('--namespace', type=str, help="Kubernetes namespace to monitor")
def main(port, namespace):
	"""
	Host the web interface.
	"""
	server = WatchdogWebServer(port=port)
	server.run_blocking()
	# asyncio.get_event_loop().run_until_complete(server.run())
