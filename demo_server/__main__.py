
import click
from .compute_backend import main as compute_backend_main
from .web import main as web_main

entrypoint = click.Group(name='kube_watchdog')
entrypoint.add_command(compute_backend_main)
entrypoint.add_command(web_main)
if __name__ == '__main__':
	entrypoint()
