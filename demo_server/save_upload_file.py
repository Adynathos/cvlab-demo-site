
from aiohttp import web
from aiofile import AIOFile, Writer
import logging
log = logging.getLogger(__name__)



async def save_upload_file(form_field, dest_path, max_size):
	size_accumulated = 0

	try:
		async with AIOFile(dest_path, 'wb') as fp:
			out_writer = Writer(fp)
			
			chunk = True
			while chunk:
				chunk = await form_field.read_chunk()  # 8192 bytes by default.
				if chunk:
					size_accumulated += chunk.__len__()

					# log.debug(f'Chunk +{chunk.__len__()}/{size_accumulated} type={type(chunk)}')

					if size_accumulated > max_size:
						log.info('Aborting file {dest_path} because size so far {size_accumulated} exceeds limit {max_size}')
						raise web.HTTPRequestEntityTooLarge(max_size, size_accumulated)

					await out_writer(chunk)		

			await fp.fsync()

	# if we abort due to a too-long file, delete the already stored file
	except web.HTTPRequestEntityTooLarge as e:
		dest_path.unlink() # remove
		raise e

	return size_accumulated
