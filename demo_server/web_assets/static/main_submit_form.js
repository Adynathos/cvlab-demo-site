"use strict";

function main() {

	const form_elem = document.getElementById('job_submission');
	const display_elem = document.getElementById('view_input_image');
	const upload_elem = document.getElementById('upload_input_image');
	const msg_submission_status = document.getElementById('submission_status');

	async function submit_image() {
		const form_data = new FormData(form_elem);

		// The file does not seem to be properly included in the FormData
		// Lets make sure it is there
		const file_obj = upload_elem.files[0];
		form_data.set('upload_input_image', file_obj);

		// We start sending the form as soon as possible
		// we don't want to wait for the promise yet, because first we display the image
		// we will wait and handle the response later
		console.log(`Submitting to ${form_elem.action}`);
		const msg_1 = `Submitting ${file_obj.name} to ${form_elem.action}.`;
		msg_submission_status.textContent = msg_1;

		const submission_promise = fetch(form_elem.action, {
			method: form_elem.method,
			body: form_data,
		});

		// display image
		// const file_obj = form_data.get('upload_input_image');
		// const file_obj = document.getElementById('input_image').files[0]; // alternate way to get file if we don't construct FormData

		const submission_response = await submission_promise;

		if(submission_response.ok) {
			console.log('Submission complete', submission_response);
			msg_submission_status.textContent = msg_1 + ' Submission complete.';
		} else {
			console.warn('Submission error', submission_response);
			msg_submission_status.textContent = msg_1 + ' Submission error.';
		}
	}

	async function update_display_file() {
		const file_obj = upload_elem.files[0];

		const file_size = file_obj.size;

		console.log('File selected', file_obj.name, 'size', file_obj.size, file_obj);

		if (file_size <= 1*1024*1024) {
			display_elem.src = URL.createObjectURL(file_obj);
			await new Promise((accept, reject) => {
				display_elem.onload = accept;
				display_elem.onerror = (ev) => {
					console.warn("File failed to load as an image", file_obj);
					msg_submission_status.textContent = `File ${file_obj.name} failed to load as an image`;
					reject(ev);
				};
			}); 

			console.log(`Loaded image of size ${display_elem.naturalWidth} x ${display_elem.naturalHeight}`);

			// if the image is ok, we submit it
			submit_image();
		} else {
			console.warn("File too big", file_size);
		}
	}

	/*---------------------------------------------------
		Dialog
	*/

	// Open file selection dialog when image is clicked
	display_elem.addEventListener('click', (event) => {
		upload_elem.click();
	});

	// When image chosen from dialog, show it
	upload_elem.addEventListener('change', (event) => {
		update_display_file();
	});

	
	/*---------------------------------------------------
		Drag/drop
	*/
	const handler = (event) => {
		event.stopPropagation();
		event.preventDefault(); 
		// console.log('handler', event);
		event.dataTransfer.dropEffect = "copy";
		return false;
	}
	// these need to be set up to prevent page redirecting to the dropped file
	document.addEventListener('dragend', handler);
	document.addEventListener('dragover', handler);

	// set image from drag-drop
	document.addEventListener('drop', (event) => {
		event.stopPropagation();
		event.preventDefault();
		upload_elem.files = event.dataTransfer.files;
		update_display_file()
	});


}


const DOM_loaded_promise = new Promise((accept, reject) => {
	if (document.readyState === 'loading') {  // Loading hasn't finished yet
		 document.addEventListener('DOMContentLoaded', accept);
	} else {  // `DOMContentLoaded` has already fired
		accept();
	}
}); 

DOM_loaded_promise.then(main);

